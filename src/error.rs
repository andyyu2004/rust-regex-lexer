use thiserror::Error;

pub trait Catch {
    type Item;
    fn catch<F>(self, f: F)
    where
        F: FnOnce(Self::Item) -> ();
}

// map_left but doesn't return anything
// Can probably be done with if let Err(e) = ...
impl<T, E> Catch for Result<T, E> {
    type Item = E;

    fn catch<F>(self, f: F)
    where
        F: FnOnce(Self::Item) -> (),
    {
        match self {
            Err(e) => f(e),
            Ok(_) => {}
        };
    }
}

pub type LexResult<T> = Result<T, LexError>;

macro_rules! impl_from(
    ($from:path, $for:ident, $variant:ident) => {
        impl From<$from> for $for {
            fn from(err: $from) -> Self {
                Self::$variant(err)
            }
        }
    }
);

impl_from!(String, LexError, UnknownSymbol);
impl_from!(Vec<LexError>, LexError, Errors);

#[derive(Error, Debug)]
pub enum LexError {
    #[error("Unable to lex unknown symbol `{0}`")]
    UnknownSymbol(String),
    #[error("{0:?}")]
    Errors(Vec<LexError>),
}
